import { Component } from '@angular/core';

@Component({
  selector: 'app-directives',
  template:`

  <!-- NGIF -->
  <h1 *ngIf ="true">
    SRIHARSHA CP
  </h1>

  <h1 *ngIf = "hello; else reyna">
    ChandlerBing
  </h1>

  <ng-template #reyna>
    <h1>Hesr illa!!!</h1>
  </ng-template>


  <!-- NGFOR -->
  <div *ngFor="let color of colors">
    <h2>{{color}}</h2>
  </div>

  <div *ngFor="let shift of shifts">
  <div [style.background]="shift.color">{{ shift.color }}</div>
  </div>


  <!-- NGIF inside NGFOR -->
  <div *ngFor="let item of items">
  <div *ngIf="item.visible">
    {{ item.name }}
  </div>
</div>



  `,
  styleUrls: ['./directives.component.scss']
})
export class DirectivesComponent {

  hello=true;
  colors=['ROSS','RACHEL GREEN','JOEY','MONICA','PHEOBE'];

  shifts:any=[
    {color:'red'},
    {color:'green'},
    {color:'black'},
    {color:'purple'},
  ]

  items = [
    { name: 'Item 1', visible: true },
    { name: 'Item 2', visible: false },
    { name: 'Item 3', visible: true }
  ];
  

}
